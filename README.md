## Instalación
1. Clonar proyecto
```bash
git clone https://gitlab.com/jacgg/scrapy_rundeck.git
# Web Escraping con scrapy de python
```

4. instalacion packages
```bash
cd scrapy_rundeck/
pip3 install -r packages.txt
```

5. iniciar scraper
```bash
scrapy crawl mlibre
```